# Mini-projet Kubernetes - Déploiement de wordpress avec persistance des données

## Source 

https://youtu.be/__3Ux0IF-Ds

## Objectif

L'objectif du projet est le déploiement de la solution wordpress sur un cluster kubernete.

Il sera ainsi question d'utiliser diff�rents objets kubernetes afin de déployer et de rendre notre application disponible

Nous utiliserons les objets tels que les deployements, les services (nodeport et clusterip), les secrets, les volumes.

L'énoncé e présenté dans la vidéo.

## My solution

These manifests are my solution to check the objectifs.

I added the `kutomization.yaml` after viewed the correction because i found this useful.

## How to deploy

```
git clone https://gitlab.com/Gribhb/eazytraining-mini-projet-kubernetes.git

cd eazytraining-mini-projet-kubernetes

kubectl apply -k ./
```

## How to clean

```
kubectl delete -k ./
```
