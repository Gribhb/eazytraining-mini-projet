> This exercice provide to https://www.youtube.com/watch?v=rfoWyUWxIVk

> Thanks to https://eazytraining.fr/ it was fun to solve this one !

## Objectives

Create an ansible role from scratch (for a webapp deployment) and deploy this one with an ansible playbook.

It would be necessary to use docker and apache for the application deployment. Ansible will be use to scale the application deployment.
