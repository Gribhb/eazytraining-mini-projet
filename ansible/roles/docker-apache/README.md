Role Name
=========

Ansible role to install docker (on ubuntu/debian hosts) and deploy an apache container.

Requirements
------------

No requirements.

Role Variables
--------------

A description of the settable variables for this role. 

Variables are in defaults/main.yml

**docker_packages** : list of packages to install on hosts needed to docker installation

**container_name** : name of the container

**container_image** : name to use for the container

**container_image_tag** : tag to use for the image

**container_port** : Internal container port

**exposed_port** : External port

**html_file** : template of a html file you want to place into */var/www/html*

**ansible_user** : user present on the host to copy the html file

Dependencies
------------

No dependencies.

Example Playbook
----------------

Example of how to use this role :

```
---
- name: Install docker and launch apache container
  hosts: staging
  user: ansible
  become: yes
  roles:
    - docker-apache
```

License
-------

BSD

Author Information
------------------

Twitter : @gribhb

Website : https://antoinemayer.fr
