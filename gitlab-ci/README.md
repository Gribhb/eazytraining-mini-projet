# eazytraining-mini-projet-gitlab-ci

> This exercice provide to https://www.youtube.com/watch?v=WMx5OE94VW0

> Thanks to https://eazytraining.fr/ it was fun to solve this one !

## Objectives

Build on application CI/CD pipeline with GitLab CI.

1. Source code of the web app + make a Dockerfile to package the application
2. Push the code on the GitLab Repository
3. Build the docker image
4. Push the docker image to the Gitlab Registry
5. Test the docker image
6. Deploy the application on staging environment 
7. Make a Pull request to deploy in production environment.

I will use AWS Beanstalk to deploy my application

## How to use this pipeline

1. Before running this pipeline you need to do some configuration on AWS.

* Create two application on Elastic Beanstalk service
    * The first one named (for example) : **Staging eazytraining**
    * The second one named : **Production eazytraining**
    * For both application select *Docker* as **Platform**

 2. Setup GitLab variables to be able to user AWS API

 * Go to Settings > CI/CD > Variables > Add variable
    * AWS_ACCESS_KEY
    * AWS_SECRET_ACCESS_KEY
    * AWS_DEFAULT_REGION

3. Setup GitLab variable to be able to use GitLab registry from AWS. We need to create a GitLab Token

* Go to Settings > Repository > Deploy Tokens
    * Declare Name (AWS for example)
    * Declare Username (AWS again)
    * Select *read_repository* and *read_registry*
    * Copy/Save the token, it will be displayed only once

* Go to Settings > CI/CD > Variables > Add variable
    * GITLAB_DEPLOY_TOKEN the value should be like this : **AWS:<your token>**

4. Create two GitLab environments

* Go to Deployment > Environments > New environment 
    * Create staging 
    * Create production

5. Create these 3 variables for both environments by selecting *Environment scope*

* Go to Settings > CI/CD > Variables > Add variable
    * APP_NAME
    * APP_ENV_NAME

6. To increase Security/Misconfiguration we can to these actions :

* Go to Settings > General > Merge requests > Merge checks : Pipelines must succeed
* Go to Settings > Repository > Protected branches > Allowed to push : No one

So, now, you're ready to run your pipeline ! :D



