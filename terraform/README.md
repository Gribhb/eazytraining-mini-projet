> This exercice provide to https://www.youtube.com/watch?v=npxuND_-Ycg

> Thanks to https://eazytraining.fr/ it was fun to solve this one !

## Objectives

Create 3 terraform modules (ec2, ebs, sg and eip) to deploy our application infrastructure. 

Need to use the module concept to subdivide the components of our infrastructure. Then we will be able to easily maintain each component independently.

* Create a module (ec2) to deploy an instance ec2 (ubuntu). And attach an ebs volume to this resource
* Create a module (ebs) to create an ebs volume (the size need to be set with a variable)
* Create a module (eip) for an public ip (need to be attach to the ec2 instance)
* Create a module (sg) to open 80 and 443 port
* Create a terraform file which will use the 4 created modules
* During the deployment, nginx need to be installed on the ec2 instance
