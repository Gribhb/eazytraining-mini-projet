terraform {

}


provider "aws" {
  profile = "default"
  region  = "eu-west-3"
}

module "sg" {
  source = "./modules/sg"

  vpc_id       = "vpc-XXXXXX"
  sg_cidr = "0.0.0.0/0"
}

module "eip" {
  source = "./modules/eip"

  instance_id = module.ec2.instance_id
}

module "ebs" {
  source = "./modules/ebs"

  instance_az = module.ec2.instance_az
  instance_id = module.ec2.instance_id
  ebs_size    = "10"
}

module "ec2" {
  source = "./modules/ec2"

  ami             = "ami-0c6ebbd55ab05f070"
  instance_type   = "t2.micro"
  ssh_public_key  = "ssh-rsa ...."
  security_groups = module.sg.sg_eazytraining
}

output "webserver_public_ip" {
  value = module.ec2.webserver_public_ip
}
