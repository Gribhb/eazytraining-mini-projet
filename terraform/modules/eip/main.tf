resource "aws_eip" "webserver" {
  instance = var.instance_id
  vpc = true
}
