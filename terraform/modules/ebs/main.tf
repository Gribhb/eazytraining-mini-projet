resource "aws_ebs_volume" "webserver_volume" {
  availability_zone = var.instance_az
  size = var.ebs_size
  
  tags = {
    Name = "webserver_ebs_volume"
  }
}

resource "aws_volume_attachment" "ebs_attachment_webserver" {
  device_name = "/dev/sdh"
  volume_id = aws_ebs_volume.webserver_volume.id
  instance_id = var.instance_id
}
