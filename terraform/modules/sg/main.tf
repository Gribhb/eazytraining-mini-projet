resource "aws_security_group" "sg_webserver" {
  name        = "sg_webserver"
  description = "WebServer Security Group - Allow HTTP/HTTPS"
  vpc_id      = var.vpc_id

  ingress {
    description      = "HTTP from all"
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = [var.sg_cidr]
    ipv6_cidr_blocks = []
  }

  ingress {
    description      = "HTTPS from all"
    from_port        = 443
    to_port          = 443
    protocol         = "tcp"
    cidr_blocks      = [var.sg_cidr]
    ipv6_cidr_blocks = []
  }  

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
}

