output "instance_id" {
  value = aws_instance.webserver.id
}

output "instance_az" {
  value = aws_instance.webserver.availability_zone
}

output "webserver_public_ip" {
  value = aws_instance.webserver.public_ip
}
