variable "ami" {
  type = string
}

variable "instance_type" {
  type = string
}

variable "ssh_public_key" {
  type = string
}

variable "security_groups" {
  type = string
}
