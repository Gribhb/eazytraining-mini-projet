resource "aws_key_pair" "eazytraining-key" {
  key_name = "eazytraining-key"
  public_key = var.ssh_public_key
}

data "template_file" "user_data" {
  template = file("${abspath(path.module)}/userdata.sh")
}

resource "aws_instance" "webserver" {
  ami = var.ami
  instance_type = var.instance_type
  key_name = aws_key_pair.eazytraining-key.key_name
  vpc_security_group_ids = [var.security_groups]
  user_data = data.template_file.user_data.rendered

  tags = {
    Name = "webserver"
  }
}


